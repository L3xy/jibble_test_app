const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const should = chai.should();


chai.use(chaiHttp);

describe('Users', () => {

  describe('/GET user', () => {
	  it('it should GET all the users', (done) => {
			chai.request(server)
		    .get('https://jsonplaceholder.typicode.com/posts')
		    .end((err, res) => {
			  	res.should.have.status(200);
			  	res.body.should.be.a('array');
			  	res.body.length.should.be.eql(0);
		      done();
		    });
	  });
  });

  describe('/POST user', () => {
	  it('it should POST a user ', (done) => {
	  	let user = {
	  		title: 'foo',
      		body: 'bar',
      		userId: 10
	  	}
			chai.request(server)
		    .post('/user')
		    .send(user)
		    .end((err, res) => {
			  	res.should.have.status(200);
			  	res.body.should.be.a('object');
			  	res.body.should.have.property('message').eql('user successfully added!');
			  	res.body.user.should.have.property('title');
			  	res.body.user.should.have.property('body');
			  	res.body.user.should.have.property('userId');
		      done();
		    });
	  });
  });
 /*
  * Test the /GET/:id route
  */
  describe('/GET userById', () => {
  	it('it should GET users by id', (done) => {
		chai.request(server)
	    .get('jsonplaceholder.typicode.com/posts/1')
	    .end((err, res) => {
		  	res.should.have.status(200);
		  	res.body.should.be.a('object');
		  	res.body.length.should.be.eql(1);
	      done();
	    });
  	});
  });
 /*
  * Test the /PUT/:id route
  */
  describe('/PUT/userById', () => {
	  it('it should UPDATE a user given the id', (done) => {
	  	chai.request(server)
	    .get('jsonplaceholder.typicode.com/posts/1')
	    .end((err, res) => {
		  	res.should.have.status(200);
		  	res.body.should.be.a('object');
		  	res.body.length.should.be.eql(1);
		  	res.body.user.should.have.property('title').eql('Updated Title');
	      done();
	    });
	  });
  });
 /*
  * Test the /DELETE/:id route
  */
  describe('/DELETE/:id user', () => {
	  it('it should DELETE a user given the id', (done) => {
	  	chai.request(server)
	    .get('jsonplaceholder.typicode.com/posts/1')
	    .end((err, res) => {
		  	res.should.have.status(200);
		  	res.body.should.be.a('object');
		  	res.body.should.have.property('message').eql('Book successfully deleted!');
			res.body.result.should.have.property('ok').eql(1);
			res.body.result.should.have.property('n').eql(1);
	      done();
	    });				  
  	});
}});
  