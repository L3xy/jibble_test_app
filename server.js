const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const port = 8080;
const crudRoutes = require('./crudRoutes/index');
const collectionRoutes = require('./collectionRoutes/index');
const validateSession = require('./middleware/validate-session');

//parse application/json and look for raw
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.text());
app.use(bodyParser.json({ type: 'application/json'}));

app.use('/api/prefix/', validateSession, crudRoutes);
app.use('/api/prefix/', validateSession, collectionRoutes);

// catch 404 and forward to error handler
app.use((req, res, next) =>{
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use((err, req, res, next) =>{
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) =>{
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


app.listen(port);
console.log("Listening on port " + port);

module.exports = app; 