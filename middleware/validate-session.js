const jwt = require('jsonwebtoken');
const constants = require('../config/constants');

module.exports = (req, res, next) => {
	let sessionToken = req.headers.authorization;

	if(sessionToken) {
		jwt.verify(sessionToken, constants.JWT_SECRET, (err, decodedId) => {
			if (decodedId) {
				console.log(decodedId.id)
				.then((user) => {
					req['user'] = user;
					next();
				}, (err) => {
					res.send(501, 'Invalid User');
				});
			} else {
				res.send(501, 'not authotized');
			}
		});	
	} else {
		res.send(501, 'Auth token absent');
	}
};