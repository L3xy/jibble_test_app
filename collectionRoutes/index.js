const router = require('express').Router();
const fetch = require('node-fetch')

function status(response) {
  if (response.status >= 200 && response.status < 300) {
    return Promise.resolve(response)
  } else {
    return Promise.reject(new Error(response.statusText))
  }
}

function json(response) {
  return response.json()
}

fetch('https://jsonplaceholder.typicode.com/posts/album/comments')
  .then(status)
  .then(json)
  .then(function(data) {
    if (data.length <= 30){
      console.log(data);
    }
  })
  .catch(function(error) {
    console.log('Fetch Error :-S', error);
  });

module.exports = router;